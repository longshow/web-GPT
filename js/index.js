window.apikey = "";
window.prompt1 = "";
window.mt = 2048;
window.tpr = 1;
window.tp = 1;
window.fp = 0;
window.pp = 0;
window.mode = "true";
window.csize = 1000;
// const textarea = window.document.getElementById("res");
const textarea = document.querySelector("#content")
// const inputbox = window.document.getElementById("inputbox");
let isHistory = true
const inputbox = document.querySelector("#input-message")
var histories = "";
addText("bot","欢迎使用ChatGPT智能AI客服,本项目基于GPT-3接口开发,能够实现与ChatGPT类似的持续性对话功能,AI将记忆并结合语境对话。");
if (getCookie("apikey") != undefined && getCookie("apikey") != "") {
    window.apikey = getCookie("apikey");
} else {
    //window.apikey = prompt("请在输入框内输入并提交您从官方网站获取的apikey:");
    if(getCookie("apikey")) {
        const keytext = document.querySelector("#keytext")
        keytext.value = getCookie("apikey")
    }
    const prom = document.querySelector("#prompt")
    prom.style.display = "block"

    setCookie("apikey", window.apikey);
}
if (getCookie("prompt1") != undefined) {
    window.prompt1 = getCookie("prompt1");
}
if (getCookie("mt") != undefined) {
    window.mt = Number(getCookie("mt"));
}
if (getCookie("tpr") != undefined) {
    window.tpr = Number(getCookie("tpr"));
}
if (getCookie("tp") != undefined) {
    window.tp = Number(getCookie("tp"));
}
if (getCookie("fp") != undefined) {
    window.fp = Number(getCookie("fp"));
}
if (getCookie("pp") != undefined) {
    window.pp = Number(getCookie("pp"));
}
if (getCookie("mode") != undefined) {
    window.mode = getCookie("mode");
}
if (getCookie("csize") != undefined) {
    window.csize = Number(getCookie("csize"));
}
function ask() {
    //let intputtext = inputbox.value;
    let intputtext = document.querySelector("#input-message").value
    if (intputtext.startsWith("/")) {
        intputtext = intputtext.substring(1);
        switch (intputtext) {
            case "help":
                addText("bot","请输入以下指令来更改ChatGPT的参数:\n\n" +
                    "/apikey  (用于设置apikey,只有在官网注册获取apikey才能正常使用本服务)\n" +
                    "/prompt  (为每次发送的文本添加前置上下文)\n" +
                    "/maxtoken  (用于控制ChatGPT每次能生成的词数.)\n" +
                    "/tpr (可以用来控制ChatGPT生成的话的多样性)\n" +
                    "/top  (可以用来控制ChatGPT生成的话的质量)\n" +
                    "/fp  (可以用来控制ChatGPT生成的话的“新颖程度”)\n" +
                    "/pp  (用于控制ChatGPT产生的句子的长度)\n" +
                    "/info  (用于显示当前各项参数的值)\n" +
                    "/mode  (用于设置是否启用持续对话模式)\n" +
                    "/csize  (用于设置每次发送给API接口的字符数)\n" +
                    "/save  (用于将设置的参数保存到浏览器的Cookie,有效期30天)");
                break;
            case "info": addText("bot",
                "当前的各项参数:\n\n" +
                "apikey:" + window.apikey + "\n\n" +
                "Prompt:" + window.prompt1 + "\n\n" +
                "Max_tokens:" + window.mt + "\n" +
                "Temperature:" + window.tpr + "\n" +
                "Top_p:" + window.tp + "\n" +
                "Frequency_penalty:" + window.fp + "\n" +
                "Presence_penalty:" + window.pp + "\n" +
                "mode:" + window.mode + "\n" +
                "csize:" + window.csize);
                break;
            case "save":
                setCookie("apikey", window.apikey);
                setCookie("prompt1", window.prompt1);
                setCookie("mt", window.mt);
                setCookie("tpr", window.tpr);
                setCookie("tp", window.tp);
                setCookie("fp", window.fp);
                setCookie("pp", window.pp);
                setCookie("mode", window.mode);
                setCookie("csize", window.csize);
                addText("bot", "参数保存成功,数据将保存30天");
                break;
            case "apikey": window.apikey = prompt("apikey 请输入从官网获取到的apikey,默认为空"); addText("bot", "设置成功"); break;
            case "prompt": window.prompt1 = prompt("Prompt 请输入要添加的上下文文本,默认为空"); addText("bot", "设置成功"); break;
            case "maxtoken": window.mt = Number(prompt("Max_tokens 请输入ChatGPT每次能生成的词数,默认为2048")); addText("bot", "设置成功"); break;
            case "tpr": window.tpr = Number(prompt("Temperature 请输入一个0到1.0的一位小数,默认为1.0")); addText("bot", "设置成功"); break;
            case "top": window.tp = Number(prompt("Top_p 请输入一个0到1.0的一位小数,默认为1.0")); addText("bot", "设置成功"); break;
            case "fp": window.fp = Number(prompt("Frequency_penalty 请输入一个-2.0到2.0的一位小数,默认为0")); addText("bot", "设置成功"); break;
            case "pp": window.pp = Number(prompt("Presence_penalty 请输入一个-2.0到2.0的一位小数,默认为0")); addText("bot", "设置成功"); break;
            case "mode": window.mode = prompt("mode 输入“true”开启持续对话模式,输入“false”关闭该模式,默认为“true”"); addText("bot", "设置成功"); break;
            case "csize": window.csize = prompt("csize 输入一个0到1000的整数,默认为1000"); addText("bot", "设置成功"); break;
            default: addText("bot", "未知指令");//alert("未知指令");
        }
        inputbox.value = "";
        return;
    }
    if (window.mode == "false") {
        histories = [];
        isHistory = false
    } else {
        isHistory = true
        histories += getCache("history") ? getCache("history") : ""
        alert(histories)
        
    }
    
    
    histories += "\n" + window.prompt1 + "\nQ:" + intputtext + "。";
    let question = "";
    if (histories.length <= window.csize) {
        question = histories;
        
    } else {
        question = histories.substring(histories.length - window.csize)
        
    }
    console.log(question);
    //addText("me", "我说：\n\n" + intputtext);
    addText("me", intputtext);
    textarea.value += '\n正在请求数据......';
    textarea.scrollTop = textarea.scrollHeight;
    axios.post('https://api.openai.com/v1/completions', {
        prompt: question, max_tokens: window.mt, model: "text-davinci-003", temperature: window.tpr,
        top_p: window.tp, frequency_penalty: window.fp, presence_penalty: window.pp
    }, {
        headers: { 'content-type': 'application/json', 'Authorization': 'Bearer ' + window.apikey }
    }).then(response => {
        let arr = textarea.value.split("\n");
        arr = arr.slice(0, arr.length - 1);
        textarea.value = arr.join("\n");
        let result = response.data.choices[0].text;
        histories += "\n" + result;
        if(isHistory) {
            setCache("history", histories)
        }
        
        let result1 = result;
        //addText("bot", "AI说:\n" + result1.replace('A:', '').replace('A：', ''));
        addText("bot", result1.replace('A:', '').replace('A：', ''));
        inputbox.value = "";
    }).catch(error => {
        console.log(error);
        //alert("服务器报错:\n" + error);
        addText("bot", "服务器报错:\n" + error)
    });
}
function addText(flag, text) {
    //textarea.value += "\n" + text + "\n" + "————————————————————";
    //textarea.scrollTop = textarea.scrollHeight;
    const content = document.querySelector("#content")
    const inputmsg = document.querySelector("#input-message")
    if (flag ===  "bot") {
        content.innerHTML +=   `
            <div class="line clearfix">
                <div class="bot-message">
                    <p>${text}</p>
                </div>
            </div>
        `
        inputmsg.value = ""
    } else if (flag === "me") {
        content.innerHTML +=   `
            <div class="line clearfix">
                <div class="me-message">
                    <p>${text}</p>
                </div>
            </div>
        `
        inputmsg.value = ""
    }
    const scrol = document.querySelector("#content")
    scrol.scrollTop = scrol.scrollHeight;
    
    
}
// 添加cookie
function setCookie(name, value) {
    let days = 10;
    var expires = '';
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toUTCString();
    }
    document.cookie = name + '=' + (value || '') + expires + '; path=/';
    setCache(name, value)
}

// 读取cookie
function getCookie(name) {
    /*var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;*/
    return getCache(name)
}

//设置缓存
function setCache(name, value) {
    localStorage.setItem(name, value)
}

//获取缓存
function getCache(name) {
    return localStorage.getItem(name)
}

//获取设置按钮元素
const keytextInput = document.querySelector("#keytext")
//获取API KEY输入框的内容
//给设置按钮元素添加绑定事件
document.querySelector("#prompt-btn").addEventListener("click", function() {
    if(keytextInput.value.trim()) {
        //设置apikey
        window.apikey = keytextInput.value.trim()
        setCookie("apikey", window.apikey);
        const prom = document.querySelector("#prompt")
        prom.style.display = "none"
    } else {
        keytextInput.value = ""
        setCookie("apikey", "");
    }
})

//复制API KEY
document.querySelector("#prompt-copy").addEventListener("click", function() {
    if(keytextInput.value.trim()) {
        //var oInput = document.createElement('input');
        //oInput.value = keytextInput.value;
        navigator.clipboard.writeText(keytextInput.value);
        /*oInput.style.opacity = 0;
        oInput.setAttribute("name","secukey")
        document.body.appendChild(oInput);
        oInput.select(); // 选择对象
        document.execCommand("Copy"); // 执行浏览器复制命令
        oInput.className = 'oInput';
        //alert('复制成功');
        document.querySelector("input[name='secukey']").remove()
        */
    }
})

//清空API KEY
document.querySelector("#prompt-clear").addEventListener("click", function() {
    keytextInput.value = ""
})

//关闭页面
document.querySelector("#prompt-close1").addEventListener("click", function() {
    const prom = document.querySelector("#prompt")
    prom.style.display = "none"
    if (!getCookie("apikey")) {
        setTimeout(function(){
            prom.style.display = "block"
        }, 500)
    }
})

//关闭页面
document.querySelector("#prompt-close2").addEventListener("click", function() {
    const prom = document.querySelector("#prompt")
    prom.style.display = "none"
    if (!getCookie("apikey")) {
        setTimeout(function(){
            prom.style.display = "block"
        }, 500)
    }
})

document.querySelector("#btn-options").addEventListener("click", function() {
    const prom = document.querySelector("#prompt")
    prom.style.display = "block"
    const keytext = document.querySelector("#keytext")
    keytext.value = getCookie("apikey")
})